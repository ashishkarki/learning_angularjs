/**
 * Created by akarki on 5/11/2015.
 */

describe("Blog Application Test", function () {
    it("should test the main blog page", function () {
        browser.get(
            "http://localhost:63342/AngularJS_ebook/app/index.html#!/");

        // first log into the application
        element(by.model("username")).sendKeys("node");
        element(by.model("password")).sendKeys("password");
        element(by.css('.form-button')).click();

        // then perform whatever tests are required
        expect(browser.getTitle()).toEqual("AngularJS Blog");
        //gets the blog list
        var blogList =
            element.all(by.repeater('blogPost in blogList'));
        //tests the size of the blogList
        expect(blogList.count()).toEqual(1);

        browser.get(
            "http://localhost:63342/AngularJS_ebook/app/index.html#!/blogPost/5394e59c4f50850000e6b7ea");
        expect(browser.getTitle()).toEqual("AngularJS Blog");

        //gets the comment list
        var commentList =
            element.all(by.repeater('comment in blogEntry.comments'));
        //checks the size of the commentList
        expect(commentList.count()).toEqual(0); // this should have returned 2 in place of 0.

        // now click the logout link and see if the application returns to login page
        element(by.css('.navbar-brand')).click();
        // logout
        element(by.id('lo')).click();
        expect(browser.getTitle()).toEqual("AngularJS Blog");
    });
});