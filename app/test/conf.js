/**
 * Created by akarki on 5/6/2015.
 */

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['e2e/blog-spec.js']
};