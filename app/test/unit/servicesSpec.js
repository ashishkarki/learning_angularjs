/**
 * Created by akarki on 5/8/2015.
 */
describe('AngularJS Blog Service Testing', function () {

        describe('test BlogList', function () {
                var $rootScope;
                var blogList;

                beforeEach(module('blogServices'));

                // $injector is used to inject services directly
                beforeEach(inject(function ($injector) {
                            $rootScope = $injector.get('$rootScope');
                            blogList = $injector.get('BlogList');
                        }
                    )
                );

                it('should test BlogList service', function () {
                    expect(blogList).toBeDefined();
                });
            }
        );

        // test BlogPost service
        describe('test BlogPost', function () {
                var $rootScope;
                var blogPost;

                beforeEach(module('blogServices'));

                beforeEach(inject(function ($injector) {
                            $rootScope = $injector.get('$rootScope');
                            blogPost = $injector.get('BlogPost');
                        }
                    )
                );

                it('should test BlogPost service', function () {
                    expect(blogPost).toBeDefined();
                });
            }
        );

        // test the login service
        describe('test login', function () {
                var $rootScope;
                var login;

                beforeEach(module('blogServices'));

                beforeEach(inject(function ($injector) {
                        $rootScope = $injector.get('$rootScope');
                        login = $injector.get('Login');
                    })
                );

                it('should test Login service', function () {
                    expect(login).toBeDefined();
                });
            }
        );
    }
);