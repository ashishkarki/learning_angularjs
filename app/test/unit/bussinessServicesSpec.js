/**
 * Created by akarki on 5/11/2015.
 */

// for now we are only checking if the setCreds service can be injected
describe('AngularJS Blog Business Service Testing', function () {
    describe('test setCreds', function () {
            var $rootScope;
            var setCreds;

            beforeEach(module('blogBusinessServices'));

            beforeEach(inject(function ($injector) {
                    $rootScope = $injector.get('$rootScope');
                    setCreds = $injector.get('setCreds');
                    setCreds("test", "test");
                })
            );

            it('should test setCreds service exist', function () {
                    expect(setCreds).toBeDefined();
                }
            );
        }
    );

// test for checkCreds service
    describe('test checkCreds', function () {
            var $rootScope;
            var checkCreds;
            var setCreds;

            beforeEach(module('blogBusinessServices'));

            beforeEach(inject(function ($injector) {
                    $rootScope = $injector.get('$rootScope');
                    checkCreds = $injector.get('checkCreds');
                    setCreds = $injector.get('setCreds');

                    setCreds("test", "test");
                })
            );

            it('should test checkCreds service exist', function () {
                    expect(checkCreds()).toEqual(true);
                }
            );
        }
    );

// test for getToken service
    describe('test getToken', function () {
            var $rootScope;
            var getToken;
            var setCreds;

            beforeEach(module('blogBusinessServices'));

            beforeEach(inject(function ($injector) {
                    $rootScope = $injector.get('$rootScope');
                    getToken = $injector.get('getToken');
                    setCreds = $injector.get('setCreds');

                    setCreds("test", "test");
                })
            );

            it('should test getToken service exist', function () {
                    expect(getToken()).toBeDefined();
                }
            );
        }
    );

// test for getUsername
    describe('test getUsername', function () {
            var $rootScope;
            var getUserName;
            var setCreds;

            beforeEach(module('blogBusinessServices'));

            beforeEach(inject(function ($injector) {
                    $rootScope = $injector.get('$rootScope');
                    getUserName = $injector.get('getUserName');
                    setCreds = $injector.get('setCreds');

                    setCreds("test", "test");
                })
            );

            it('should test getUsername service exist', function () {
                    expect(getUserName()).toEqual("test");
                }
            );
        }
    );

// test for deleteCreds
    describe('test deleteCreds', function () {
            var $rootScope;
            var deleteCreds;
            var setCreds;
            var checkCreds;

            beforeEach(module('blogBusinessServices'));

            beforeEach(inject(function ($injector) {
                    $rootScope = $injector.get('$rootScope');
                    deleteCreds = $injector.get('deleteCreds');
                    setCreds = $injector.get('setCreds');
                    checkCreds = $injector.get('checkCreds');

                    setCreds("test", "test");
                    deleteCreds();
                })
            );

            it('should test deleteCreds service exist', function () {
                    expect(checkCreds()).toEqual(false);
                }
            );
        }
    );

});