/**
 * Created by akarki on 5/11/2015.
 */

'use strict';

// All our Directives here
var blogDirectives = angular.module('blogDirectives', []);

blogDirectives.directive('blgMenu', function () {
    return {
        restrict: 'A',
        templateUrl: 'partials/menu.html',
        link: function (scope, el, attrs) {
            scope.label = attrs.menuTitle;
        }
    };
});