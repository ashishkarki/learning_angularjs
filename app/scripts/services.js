/**
 * Created by akarki on 5/6/2015.
 */

'use strict';

/* Define all my services here */
var blogServices = angular.module('blogServices', ['ngResource']);

// BlogPost is the name of our RESTful service
blogServices.factory('BlogPost', ['$resource',
    function ($resource) {
        return $resource("http://nodeblog-micbuttoncloud.rhcloud.com/NodeBlog/blog/:id", {}, {
                get: {method: 'GET', cache: false, isArray: false},
                save: {method: 'POST', cache: false, isArray: false},
                update: {method: 'PUT', cache: false, isArray: false},
                delete: {method: 'DELETE', cache: false, isArray: false}
            }
        );
    }
]);

// this new service returns a list of blogs
blogServices.factory('BlogList', ['$resource',
    function ($resource) {
        return $resource
        ("http://nodeblog-micbuttoncloud.rhcloud.com/NodeBlog/blogList",
            {}, {
                get: {method: 'GET', cache: false, isArray: true}
            });

    }
]);

// this service validates login
blogServices.factory('Login', ['$resource',
        function ($resource) {
            return $resource("http://nodeblog-micbuttoncloud.rhcloud.com/NodeBlog/login",
                {}, {login: {method: 'POST', cache: false, isArray: false}}
            );
        }
    ]
);