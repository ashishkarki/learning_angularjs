/**
 * Created by akarki on 5/6/2015.
 */

/* Jasmine specs for controllers go here */
describe('Hello World', function() {

    beforeEach(module('blogApp'));

    describe('MainCtrl', function(){
        var scope, ctrl;
        beforeEach(inject(function($rootScope, $controller) {
            scope = $rootScope.$new();
            ctrl = $controller('MainCtrl', {$scope: scope});
        }));

        it('should create initialed message', function() {
            expect(scope.message).toEqual("Hello World");
        });

    });

    describe('ShowCtrl', function(){
        var scope, ctrl;

        beforeEach(inject(function($rootScope, $controller) {
            scope = $rootScope.$new();
            ctrl = $controller('ShowCtrl', {$scope: scope});
        }));

        it('should create initialed message', function() {
            expect(scope.message).toEqual("Show The World");
        });

    });

    describe('CustomerCtrl', function(){
        var scope, ctrl;

        beforeEach(inject(function($rootScope, $controller) {
            scope = $rootScope.$new();
            ctrl = $controller('CustomerCtrl', {$scope: scope});
        }));

        it('should create initialed message', function() {
            expect(scope.customerName).toEqual("Drunken Clam");
        });
    });

    // test the LoginCtrl controller used for login pages
    describe('LoginCtrl', function () {
        var scope, ctrl;

        beforeEach(inject(function ($rootScope, $controller) {
            scope = $rootScope.$new();
            ctrl = $controller('LoginCtrl', {$scope: scope});
        }));

        it('should show submit success', function () {
            console.log("LoginCtrl:" + scope.sub);
            expect(scope.sub).toEqual(true);
        });
    });

    // test the LogoutCtrl controller used to logout
    describe('LogoutCtrl', function () {
        var scope, ctrl;

        beforeEach(inject(function ($rootScope, $controller) {
            scope = $rootScope.$new();
            ctrl = $controller('LogoutCtrl', {$scope: scope});
        }));

        it('should create LogoutCtrl controller', function () {
            console.log("LogoutCtrl:" + ctrl);
            expect(ctrl).toBeDefined();
            //expect(scope.blogList).toBeUndefined();
        });
    });
});