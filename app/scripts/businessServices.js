/**
 * Created by akarki on 5/11/2015.
 */
'use strict';

// only put business service (like authentication) logic here
var blogBusinessServices = angular.module('blogBusinessServices', ['ngCookies']);

blogBusinessServices.factory('checkCreds',
    ['$cookies', function ($cookies) {
        return function () {
            var doCredsExist = false;
            var blogCreds = $cookies.blogCreds;

            if (blogCreds !== undefined && blogCreds !== "") {
                doCredsExist = true;
            }

            return doCredsExist;
        };
    }]
);

blogBusinessServices.factory('getToken',
    ['$cookies', function ($cookies) {
        return function () {
            var blogToken = "";
            var blogCreds = $cookies.blogCreds;

            if (blogCreds !== undefined && blogCreds !== "") {
                blogToken = btoa(blogCreds);
            }

            return blogToken;
        };
    }]
);

blogBusinessServices.factory('getUserName',
    ['$cookies', function ($cookies) {
        return function () {
            var userName = "";
            var blogUserName = $cookies.blogUserName;

            if (blogUserName !== undefined && blogUserName !== "") {
                userName = blogUserName;
            }

            return userName;
        };
    }]
);

blogBusinessServices.factory('setCreds',
    ['$cookies', function ($cookies) {
        return function (un, pw) {
            var token = un.concat(":", pw);
            $cookies.blogCreds = token;
            $cookies.blogUserName = un;
        };
    }]
);

blogBusinessServices.factory('deleteCreds',
    ['$cookies', function ($cookies) {
        return function () {
            $cookies.blogCreds = "";
            $cookies.blogUserName = "";
        };
    }]
);