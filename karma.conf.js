module.exports = function (config) {
    config.set({

        basePath: './',

        files: [
            "app/scripts/lib/angular.min.js"
            , "app/scripts/lib/angular-mocks.js"
            , "app/scripts/lib/angular-route.min.js"
            , "app/scripts/lib/angular-resource.min.js" /* only required when working with REST services */
            , "app/scripts/lib/angular-cookies.min.js" /* Required for authentication SERVICE as it uses cookies*/

            , "app/test/**/*Spec.js"
            , "app/partials/*.html"

            , "app/scripts/controllers.js"
            , "app/scripts/services.js"
            , "app/scripts/businessServices.js"
            , "app/scripts/directives.js"
            , "app/app.js"
        ],

        preprocessors: {
            "app/partials/*.html": ["ng-html2js"]
        },

        autoWatch: true,

        frameworks: ['jasmine'],

        browsers: ['Chrome'],

        plugins: [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter',
            'karma-ng-html2js-preprocessor'
        ],

        junitReporter: {
            outputFile: 'test_out/unit.xml',
            suite: 'unit'
        },

        ngHtml2JsPreprocessor: {
            stripPrefix: 'app/partials/'
        }
    });
};
